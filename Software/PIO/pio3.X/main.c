/*
 * Main file of gpio3.X
 * Author: mosee
 *
 * A LED is connected to RA4 and a push-button is connected to RB14.
 * When the button is pressed, the LED is turned on for 5s.
 * The button has no effect during this 5s.
 * 
 * Created on 27 January 2021, 11:32
 */


#define FCY 3685000     // �cycle frequency. Needed for __delay_ms
#include "libpic30.h"   // Contains __delay_ms definition
#include "xc.h"         // Contains register variable definitions

int main(void) {
    _TRISA4 = 0;    // Configure the LED pin as a digital output pin
    _TRISB14 = 1;   // Configure the button pin as a digital input pin

    // Main (infinite) loop
    while(1) {
        if (_RB14) {
            _LATA4 = 1;
            __delay_ms(5000);
            _LATA4 = 0;
        }
    }
    
    return 0;
}

