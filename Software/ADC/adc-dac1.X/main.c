 /*
 * Main file of ADC-DAC1.X
 * Author: mosee
 *
 * Implémentation de l'exercice ADC-DAC en utilisant le déclenchement manuel
 * des conversions.
 * La fréquence d'echantillonnage est la plus precise possible pour le dsPIC,
 * car les conversions sont entierement gerees par le hardware.
*/
#include "xc.h"
#include "adc.h"
#include "mcp4821.h"


/* Timer3 ISR
 * Called automatically when T3IF is set.
 * NEVER CALL AN ISR ANYWHERE IN THE CODE
 */
void __attribute__((interrupt, no_auto_psv))_T3Interrupt(void) {
    _T3IF = 0;
    adcStart();
}



int main(void) {
    int16_t voltage;
    
    // Configuration de L'ADC pour utilisation en polling sur AN0
    adcInit(ADC_MANUAL_SAMPLING);
    mcp4821Init(MCP4821_GAIN_X2);

    PR3 = 368;          // T=100us=(PR1+1)/3.685MHz => PR1=368
    _T3IE = 1;          // on active l'interruption du TIMER3
	T3CONbits.TON = 1;
    
	while(1) {
        if ( adcConversionDone() ) {    // On attend la fin de la conversion
            voltage = adcRead();        // On lit le resultat
            mcp4821Write(voltage);      // On l'envoie au DAC
        }
	}
}
