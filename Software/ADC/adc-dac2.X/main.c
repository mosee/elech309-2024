/*
 * Main file of ADC-DAC2.X
 * Author: mosee
 *
 * Implémentation de l'exercice ADC-DAC en utilisant le déclenchement hardware
 * par le TIMER3.
 * La fréquence d'echantillonnage est la plus precise possible pour le dsPIC,
 * car les conversions sont entierement gerees par le hardware.
*/

#include "xc.h"
#include "adc.h"
#include "mcp4821.h"


int main(void) {
    int16_t voltage;
    
    adcInit(ADC_TIMER3_SAMPLING);
    mcp4821Init(MCP4821_GAIN_X2);

    PR3 = 368;                 // T=100us=(PR1+1)/3.685MHz => PR1=368
	T3CONbits.TON = 1;
    
	while(1) {
        if ( adcConversionDone() ) {    // On attend la fin de la conversion
            voltage = adcRead();        // On lit le resultat
            mcp4821Write(voltage);      // On l'envoie au DAC
        }
	}
}
