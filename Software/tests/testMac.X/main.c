/*
 * Main file of voltmetre2.X
 * Author: mosee
 *
 */

#include <xc.h>
#include <libq.h> /* include fixed-point library */
#include <math.h>


#define N   16

int32_t mac16(int16_t *coeff, int16_t *samples);


int main(void) {
    int16_t coeff16[N];
    int16_t samples[N];
    int32_t result;

    int16_t i;
    
    _TRISB6 = 0;
    
    for (i=0; i<N; i++) {
        coeff16[i] = 512*sin((2*M_PI*i)/N);
        samples[i] = 511 + coeff16[i];
    }
	
    while(1) {
        result = mac16(coeff16, samples);
    }
}

int32_t mac16(int16_t *coeff, int16_t *samples) {
    int16_t i;
    int32_t result;
    
    _LATB6 = 1;
    result = 0;
    for (i=0; i<N; i++) {
        result += (int32_t)coeff[i]*samples[i];
        //result += __builtin_mulss(coeff[i],samples[i]);
    }
    _LATB6 = 0;
    
    return(result);
}