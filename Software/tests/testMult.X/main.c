/*
 * Main file of voltmetre2.X
 * Author: mosee
 *
 */

#include "xc.h"


int main(void) {
    int16_t a16 = 1000, b16 = 250;
    int32_t a32, b32, c;  
    float af, bf, cf;
    
    a32 = a16;
    b32 = b16;
    af = a16;
    bf = b16;
    
    c = a16*b16;                    // 16x16 multiplication
    LATB = c;
    c = (int32_t)a16*b16;           // 16x16 multiplication with casting
    LATB = c;
    c = a32*b32;                    // 32x32 multiplication
    LATB = c;
    cf = af*bf;                     // float multiplication
    LATB = (int)cf;
    c = __builtin_mulss(a16,b16);   // Hardware-dependant function
	LATB = c;
    while(1);
}
