# Utilisation des PIO

## Objectifs

- Découvrir un microcontrôleur d’une famille couramment utilisée : les PIC de Microchip
- Comprendre la structure de base d'un programme pour microcontrôleur
- Utiliser des entrées-sorties parallèles (PIO).

## Blink

Cet exemple sera votre premier projet utilisant un dsPIC.  
Vous allez réaliser un montage permettant de faire clignoter une LED.  C'est l'équivalent du "Hello World" pour l'électronique embarquée.

Un projet d'électronique embarquée est généralement divisé en 2 parties : le *hardware* et le *software*.

### Hardware

Le hardware est généralement décrit par un schéma électronique.  Dans notre exemple, nous utiliserons le module décrit dans le document ["Module MC802"](../Hardware/module_MC802.md).  

![Hardware du projet Blink](./img/Blink-HW.svg){width=50%}

A ce module, nous ajoutons une LED (D1) qui sera contrôlée par la patte **RA4** du dsPIC.  Son courant est limité par **R1**.  Le calcul de sa valeur est donné en annexe.

### Software

Le cahier des charges du code est simple : on désire que la LED clignote à 1Hz.

Vous devez d'abord installer les outils logiciels nécessaires. La procédure à suivre est décrite dans le document [Installation de MPLAB X](../Guides/Installation_MPLABX.md).

## Les PIO

Avant de vous expliquer le code, il faut s'intéresser au circuit qui contrôle les pattes du microcontrôleur.

Les pattes d'entrée/sortie d'un µC peuvent souvent jouer plusieurs rôles.  C'est pour cela qu'elles ont plusieurs noms sur le symbole du dsPIC.  Ces différents rôles sont liés aux périphériques internes du µC.

**PIO** est l'acronyme de *General Purpose Input/Output*. C'est l'appellation habituelle pour le rôle le plus simple que peut prendre une patte d'I/O d'un µC.

On trouve également la dénomination *Parallel Input/Output* (**PIO**), entre autres dans la notice du dsPIC.  Cette appellation vient du fait que les **GPIO** sont groupées en ports, pour être manipulées plus facilement par le µC.
La figure ci-dessous (venant de la notice du dsPIC) montre le schéma-bloc du circuit logique d'une patte d'I/O :

![Schéma-bloc d'une **PIO**](./img/PIO-block-diagram.png){width=75%}

Examinons les différentes parties de ce schéma :

- Dans le bloc **I/O**, on trouve la patte proprement dite (*I/O pin*) : le petit carré contenant une croix.  Elle est directement connectée à un buffer de sortie (tri-state) et un buffer d'entrée (à trigger de Schmitt).
- Le bloc **Output Multiplexers** définit qui contrôle le buffer de sortie : la patte peut servir de sortie à un périphérique du dsPIC ou être connectée au **PIO Module** pour servir de **PIO**.  Par défaut, les multiplexeurs sont configurés pour le rôle **PIO**.
- Le **Peripheral Module** montre les connexions vers les périphériques qui peuvent être connectés à cette patte.  Il ne nous intéresse pas pour l'instant.
- Lorsque le **PIO Module** est connecté aux buffers de la patte, l'*Output Enable* du buffer de sortie est alors contrôlé par **Tris Latch** (*TRIS* est l'abréviation de TRIState).  C'est donc l'état de ce bistable qui décide si la patte est une entrée (1) ou une sortie (0).
  - Dans le cas où **Tris Latch** vaut 0, l'état logique de la patte est défini par le bistble **Data Latch**.
  - Dans le cas où **Tris Latch** vaut 1, l'état logique de la patte est défini par le "monde extérieur".

Ces deux bistables sont connectés au bus de données (*Data Bus*) du dsPIC.

Les bistables **Tris Latch** et **Data Latch** de plusieurs pattes sont groupés pour former des registres **SFR** du µC.  Cela nous permet de contrôler plusieurs pattes en même temps, d'où le nom de *Parallel I/O*.  Les pattes contrôlées ensemble forment ce qu'on appelle un *port*.

Notre dsPIC possède 2 *ports* : **PORTA** et **PORTB**.  **PORTA** regroupe les pattes **RA0** à **RA4** et **PORTB** les pattes **RB0** à **RB15**.

Chaque port a 3 registres :

- **TRIS**, il est composé des *Tris Latch* et définit quelles pattes du port sont des sorties et lesquelles sont des entrées.  Le rôle de chaque patte peut être choisi individuellement.  Par défaut, toutes les pattes sont des entrées.
- **LAT**, il est composé des *Data Latch* et définit l'état de la patte dans le cas où elle est une sortie.  Dans le cas où elle est une entrée, son état n'a pas d'influence sur la patte.  Son état par défaut est indéterminé.
- PORT, dont l'utilisation est plus subtile :
  - Écrire dans **PORT** a le même effet qu'écrire dans **LAT**.
  - Lire dans **PORT** permet de récupérer l'état logique de la patte elle-même.

### Utiliser les PIO dans un programme

Notre code doit pouvoir modifier l'état des registres associés aux **PIO**.  Pour cela, une variable est associée à chaque registre du dsPIC (ces variables sont définies dans *xc.h*).  Ces variables (de type `uint16_t`) portent le même nom que le registre auquel elles sont associées.

Pour les PIO, nous en avons donc 3 : **TRISx**, **LATx** et **PORTx**.

Dans la notice du dsPIC, les 'x' minuscules dans les noms de registres indiquent qu'ils en existent plusieurs.  Dans le cas des **PIO**, nous avons **TRISA**, **LATA**, **PORTA** et **TRISB**, **LATB**, **PORTB**.  Les noms des variables associées aux registres sont toujours en majuscules.

De plus, il existe en général une autre variable de type *structure* qui permet d'accéder à chaque bit de son registre associé séparément.  C'est pratique car, souvent, on veut pouvoir agir sur une seule patte.  Le nom de ces structures est le nom du registre (en majuscule) suivi de 'bits' (en minuscule).  Le nom des champs de ces structures est le même que dans la notice du dsPIC

Par exemple, les lignes de code ci-dessous configure la patte RA4 en sortie à l'état haut :

```C
TRISAbits.TRISA4 = 0;
LATAbits.LATA4 = 1;
```

## Blink, l'explication

Vous trouverez le projet *MPLAB X* correspondant dans le dossier [Software/PIO](../Software/PIO).

Après toutes ces explications, nous pouvons maintenant expliquer le fonctionnement de notre programme :

```C
#define FCY 3685000    // Main clock frequency. Needed for __Delay_ms
#include "libpic30.h"  // Contains __delay_ms definition
#include "xc.h"        // Contains register variable definitions

int main(void) {
    // Initial setup and hardware configuration
    TRISAbits.TRISA4 = 0;    // Configure RB15 as a digital output pin

    // Main (infinite) loop
    while(1) {
        LATAbits.LATA4 = 1;   // turn the LED on
        __delay_ms(500);      // wait for 500ms
        LATAbits.LATA4 = 0;   // turn the LED off
        __delay_ms(500);      // wait for 500ms
    }
    
    return 0;
}
```

### Structure générale

- La première ligne définit la valeur de la vitesse d'exécution (la fréquence des cycles-machines).  Cette valeur sera utilisée pour fixer la fréquence de clignotement de la LED.
- Les 2 lignes suivantes incluent des fichiers d'en-tête.  C'est l'équivalent du *import* de **Python**.
  - La librairie *libpic30* contient des fonctions spécifiques aux dsPIC.  Elle contient en particulier la fonction `__delay_ms()` que nous utiliserons pour définir la durée des états de la LED.  C'est cette fonction qui a besoin de la définition de **FCY**.
  - Le fichier *xc.h* contient la définition de toutes les variables associées aux *SFR* du dsPIC.  Il doit donc être inclus dans tous nos projets.
- Tous les programmes en C ont une fonction appelée `main` qui est appelée au début du programme. Elle est généralement divisée en 2 parties :
  - Elle commence par une phase d'initialisation dans laquelle on configure le mode de fonctionnement des périphériques (c'est l'équivalent du `setup()` d'Arduino).  
  Dans notre cas, on y configure la patte **RA4** en sortie en modifiant le bit correspondant du registre **TRISA**.  
  On n'a pas à se préoccuper des périphériques qu'on n'utilise pas. Par défaut, ils sont désactivés.
  - Après la phase d'initialisation, on trouve une boucle infinie, généralement appelée boucle principale (équivalent de la fonction `loop()` d'Arduino).  Ici, on utilise une boucle `while`.  
  Votre programme doit **toujours** se terminer par une boucle infinie, même si elle ne contient pas de ligne de code.  
  En effet, un processeur doit toujours avoir du code à exécuter.  Dans notre cas, si le dsPIC arrive à la fin de `main()`, il ne sait plus quoi faire et il redémarre le programme.  
  Notre boucle principale est assez simple : elle met **RA4** à 1, attend 0.5s, met **RA4** à 0 et attend 0.5s.  
  Ensuite, la boucle recommence.
- La dernière ligne de code est en fait inutile puisque le programme ne l'atteindra jamais.  
C'est un héritage du langage C standard : la fonction `main()` est définie comme étant de type `int`, elle doit donc avoir une instruction `return`.

## Exercices

Voici quelques propositions d'exercices utilisant les **PIO**.  Vous trouverez nos solutions dans le dossier [Software/PIO](../Software/PIO).

### Copie d'un projet existant

Avant que vous ne vous lanciez, voici une petite astuce : vous pouvez créer une copie d'un projet existant, cela vous évitera d'en recréer un à chaque fois :

- Ouvrez le projet sur lequel vous voulez vous baser (*pio1.X* par exemple).
- Cliquez-droit sur le nom du projet dans le gestionnaire de projet, et choisissez *copy…* dans le menu déroulant.
- Dans la fenêtre de dialogue, indiquez le nom et l'emplacement du nouveau projet.

Remarquez que vous avez maintenant deux projets ouverts.  *MPLAB X* supporte cela sans problème.  Néanmoins, pensez à fermer (Clic-droit sur le nom du projet/Close) les projets dont vous n'avez plus besoin.  Cela vous évitera de modifier le mauvais main.c ou de transférer le mauvais code dans votre dsPIC…

### Modification du hardware par l'ajout d'un bouton

Nous allons ajouter un bouton à notre circuit pour pouvoir interagir avec le dsPIC :

![Module MC802 connecté à une LED et un bouton](./img/PIO-LED-button.svg){width=50%}

Lorsque le bouton est enfoncé, il connecte la patte au 3,3V.  
Dans le cas contraire, l'état de la patte est défini par **R2**.  En effet, si la patte est une entrée, elle ne fournit aucun courant et la tension aux bornes de **R2** est donc nulle.

### Connexion directe (pio2.X)

On veut que la LED s'allume lorsque le bouton est enfoncé et s'éteigne lorsqu'il est relâché.

### Minuterie (pio3.X)

- Par défaut, la LED est éteinte.
- Lorsque le bouton est enfoncé, la LED doit s'allumer et rester allumée pendant 5 secondes, quel que soit le moment où le bouton est relâché.
- Si le bouton est à nouveau enfoncé pendant ces 5 secondes, cela n'a pas d'effet.
- Si le bouton est enfoncé à la fin des 5 secondes, la LED est à nouveau allumée 5 secondes.

### Bistable Toggle (pio4.X)

- Initialement, la LED est éteinte.
- Chaque fois que le bouton est enfoncé, la LED doit changer d'état.

## Annexe : dimensionnement du circuit de la LED

La notice de la LED choisie (SLR-343PC) nous indique que son courant nominal est de 10mA, pour une tension de 2,1V.
La notice du dsPIC nous fournit le graphique suivant :

![Caractéristique de sortie de **RA4**](./img/PIO-ouput-characteristic.png)

Elle représente la relation entre le courant fourni par une PIO et sa tension de sortie lorsqu'elle est à l'état haut.  On voit que si on veut 10mA, la tension de sortie sera d'environ 2,9V.

On obtient donc :
```math
R1=\frac{2,9V-2.1V}{10mA}=80Ω
```

La valeur standard la plus proche est 82Ω.
