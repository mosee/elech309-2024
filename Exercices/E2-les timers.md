# Les timers

## Introduction

Dans les exercices sur les **PIO**, nous avons fait clignoter une LED en utilisant `__delay_ms()` pour fixer la période.

Cette méthode est simple à implémenter.  Elle a cependant 2 défauts :

- La période de clignotement n'est pas exactement égale au délai, car il faut tenir compte du temps d'exécution total de la boucle principale.  
Pour l’exercice sur la LED, c'est négligeable, mais pour des applications demandant plus de précision et/ou une période plus courte, cela peut être gênant.
- Pendant l'exécution de `__delay_ms()`, le µC ne fait rien d'autre.  Cela empêche le µC de se consacrer à d'autres tâches.

En théorie, on peut ajouter du code dans la boucle principale pour que le µC fasse autre chose et diminuer le délai pour tenir compte du temps d'exécution de la boucle principale.

En pratique, cela peut vite devenir compliqué : s'il y a des conditions dans le code (if, while, for…), le temps d'exécution de la boucle n'est pas constant, sauf si on ajoute des délais aux bons endroits.  De plus, il faut adapter tous ces délais à chaque modification.

Heureusment, il existe des méthodes plus efficace pour obtenir des timings précis.

## Cahier des charges

Pour illustrer ces problèmes et les solutions possibles, nous allons essayer de satisfaire le cahier des charges suivant :

- Le µC doit générer un signal à la fréquence de 500Hz, avec un rapport cyclique  de 50%.
- D'autre part, le µC doit également exécuter une autre tâche dont l'exécution prend quelques ms.  
Cela peut par exemple être le calcul d'un filtre numérique, le contrôle d'un moteur…  
Dans nos codes, cette tâche sera simplement simulée par une fonction contenant des délais.

## Exercice 1 : Utilisation d'un délai

Dans le dossier [Software/timer](../Software/timer/), vous trouverez le projet *timer1.X* qui contient une implémentation du cahier des charges en utilisant `__delay_ms` (basé sur *pio1.X*).

- Programmez votre dsPIC avec ce code et observez le signal de **RB15** avec votre Picoscope.
- Remarquez que la fréquence du signal n'est ni correcte, ni fixe.
- Expliquez d'où vient ce comportement.  Pour cela, observez également **RB13**.  Son état haut correspond à l'exécution de `aLongTask()`.

## Les timers

Les µC possèdent un périphérique pour gérer efficacement le temps : le *timer*.  
Notre dsPIC possède 5 timers, nous utiliserons le *timer1*.

Un timer compte les périodes d'horloge du µC, jusqu'à atteindre un seuil défini (par le code).  Il recommence alors à compter en partant de 0 (on dit que le timer déborde).  Au même moment, il met à 1 un bit appelé *flag d'interruption*.

Tous les périphériques possèdent un ou plusieurs flags d'interruption. Ceux-ci sont "levés" (mis à 1) lorsqu'un évènement particulier se produit (dans le cas des timers, lors d'un débordement).

Ils permettent de synchroniser l'exécution d'un code avec des évènements qui se produisent indépendament de l'exécution du code.  On s'en sert de deux manières : le *polling* et l'*interruption*.

- Lisez le document *Section 11. Timers* du [manuel de référence de la famille dsPIC33F]() pour comprendre le fonctionnement du timer1 et comment le configurer.

Remarque : les timers ont plusieurs modes de fonctionnement. Celui qui nous intéresse est le *timer mode*.

Comme pour les **PIO** (et tous les périphériques), la manipulation des timers se fait via des registres.  La figure ci-dessous montre comment ces registres sont décrits dans la documentation :

![Les registres du timer1](./img/T1CON.png){width=75%}

Il s'agit du registre de configuration du timer (*Timer x CONfiguration*).  Comme tous les registres du dsPIC, c'est un registre 16 bits.

Le nom de chaque bit est défini par ce tableau (et décrit dans la notice).

Par exemple, le bit 15 est **TON**.  Il permet de démarrer/arrêter le timer. Cette ligne de code démarre le timer1 :

```C
T1CONbits.TON = 1;
```

Les bits 4 et 5 forment un champ.  Ils configurent le *prescaler* du timer qui peut prendre 4 valeurs différentes.

La plupart des bits ont une valeur par défaut indiquée au dessus, avec son type.

Pour configurer un timer, vous devez répondre aux questions suivantes :

- Quel(s) bit(s) de **TxCON** devez-vous modifier ?
- Quelle valeur doit contenir **PRx** pour que le timer déborde à la bonne fréquence ?

## Utilisation d'un timer en polling

### Principe du polling

La méthode la plus simple pour utiliser un timer est le polling.

Elle consiste à vérifier l'état du flag d'interruption qui nous intéressent dans la boucle principale.  Cette vérification est très rapide et ne bloque donc pas l'exécution du code.  Elle est faite à chaque exécution de la boucle principale.

Le principe de fonctionnement est le suivant :

- Le timer déborde et lève son flag
- La boucle principale, qui vérifie l'état du flag, détecte sa levée
- Elle effectue l'action prévue (dans notre cas, changer l'état de la LED)
- Elle baisse le flag (le remet à 0) pour indiquer qu'elle a réagi à l'évènement.

Les 2 dernières étapes peuvent être inversées.  

Les flags d'interruption sont regroupés dans les registres **IFSx** (*Interrupt Flag Status*).  Le flag du timer1 se trouve dans le registre **IFS0**.

### analogie postale

Pour illustrer ce principe, on utilise souvent une analogie avec les boîtes aux lettres américaines (qui aurait inspiré l'idée du polling aux informaticiens).

Les boites aux lettres américaines ont souvent un petit "drapeau" qui peut être levé ou baissé.

![Boite aux lettres "américaine"](./img/mailbox.jpg)

C'est utile pour les gens dont la maison est éloignée de la route et qui ont donc une "grande" distance à parcourir pour aller récupérer leur courrier.

Lorsque le facteur dépose du courrier dans la boite, il lève le drapeau.

Il n'est alors plus nécessaire d'aller jusqu'à la boite pour savoir s'il y a du courrier, un coup d'œil par la fenêtre suffit.

Après avoir pris son courrier, on baisse simplement le drapeau.

Dans notre cas, le facteur, c'est le timer1 (ou tout autre périphérique), le courrier, c'est l'information qu'il y a eu un débordement et le propriétaire de la boite, c'est le µC.

Remarque : Le facteur ne baisse jamais le drapeau.  Si le drapeau est levé quand il passe, le facteur n'y touche pas.  
De même, un périphérique ne remet jamais un flag d'interruption à 0.

### Code informatique

Si on traduit ce principe en code, nous obtenons la structure suivante :

```C
int main() {
	// Ajouter ici la configuration du timer1

	while(1) {
		if (IFS0bits.T1IF == 1) {
			IFS0bits.T1IF = 0;
			// Ajouter le code à exécuter lorsque le timer1 déborde
		}
		// Ajouter ici les autres actions (aLongTask)
	}
	return 0
}
```

## Exercice 2 : Utilisation du timer1 en polling

- En vous basant sur la structure ci-dessus, modifiez *timer1.X* pour générer l'onde à 50kHz en utilisant le timer1 en polling.
- Observez **RB15** et **RB13**.

**RB13** est quasi tout le temps à l'état haut. En effet, le polling de **T1IF** prend très peu de temps.

Au lieu de mettre **RB13** à l'état haut pendant l'exécution de `aLongTask()`, faites juste changer son état avant d'appeler cette dernière, ce sera plus lisible.

Le signal de **RB15** n'est toujours pas parfait. On peut toutefois remarquer, qu'en moyenne, sa période est correcte (utilisez la mesure de fréquence du Picoscope pour le vérifier).

- Expliquez les formes d'ondes que vous avez obtenues.

## Exercice 3 : Utilisation du timer1 en interruption

Le problème dans le code précédent est que le délai entre le débordement du timer1 et le changement de RB15 peut varier de 0 au temps d'exécution de la boucle principale.  
Ce délai est appelé la latence.

Cela crée une imprécision sur l'instant où se produisent les flancs du signal (on appelle cela du *jitter*).  Pour éviter cela, il faut que la latence soit constante.  
C'est ce que permet le mécanisme des interruptions.

Pour utiliser une interruption, le code configure le µC pour exécuter automatiquement une fonction lorsque le flag d'interruption lié à un événement spécifique est activé.  
Le µC interrompt alors l'exécution du programme principal et exécute cette fonction.  
Elles est appelée *routine de service d’interruption* ou **ISR** (*Interrupt Service Routine*).

Les ISR sont des fonctions particulières :

-  Elles ont une syntaxe spécifique (voir ci-dessous).
- Elles ne doivent **jamais** être appelée par le code.
- Elles doivent remettre à zéro leur flag d’interruption pour indiquer que l'évènement a été traité (comme dans le cas du polling).

Pour appliquer cette technique à notre cas, la procédure est :

- La configuration du timer1 est la même que pour le polling
- A la fin de cette configuration, il faut activer l’interruption du timer1.  Cela se fait en mettant à ’1’ le bit *Interrupt Enable* qui lui correspond.  Dans le cas du timer1, c'est **IEC0bits.T1IE**.
- Il faut supprimer tout ce qui concerne le timer1 et **RB15** dans la boucle principale.  
Ce n'est plus elle qui s'en occupe, mais l'ISR.
- L’ISR correspondant au traitement de l’événement doit être écrite.  
Chaque ISR a un nom prédéfini indiquant au compilateur à quel flag d'interruption elle doit être liée (voir code ci-dessous).  
Notre ISR doit contenir le code qui gère **RB15**.

Cela nous donne la structure du code suivante :

```C
int main() {
	// Ajouter ici la configuration du timer1 et l'activation de T1IE

	while(1) {
		// il n'y a rien à faire concernant le timer 1 et RB15 dans la 
		// boucle principale.
		// Ajouter ici les autres actions (aLongTask)
	}
	return 0
}


void __attribute__((interrupt, no_auto_psv))_T1Interrupt(void) {
    IFS0bits.T1IF = 0;
    // Ajouter le code à exécuter lorsque le timer1 déborde
}
```

Le principe de fonctionnement de ce code est :

- La boucle principale est exécutée normalement
- En parallèle, le timer1 compte
- Lorsque le timer1 déborde, il lève son flag d'interruption.
- Comme l'interruption est activée (**T1IE** = 1), le µC interrompt l'exécution de la boucle principale et commence à exécuter l'ISR du timer1
- L'ISR doit baisser le flag d'interruption pour indiquer que l'évènement a été traité.
- Lorsque l'ISR se termine, le µC reprend l'exécution de la boucle principale.
La latence de l'interruption (le délai entre le débordement et le début de l'exécution de l'ISR) est constante car il est dû au circuit logique qui gère les interruptions dont le comportement est complètement déterministe.
