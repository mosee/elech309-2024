# ADC et DAC

## Le convertisseur analogique-numérique

Un convertisseur analogique-numérique (ADC) a pour rôle de transformer une tension analogique (évoluant de manière continue) en un nombre binaire codé sur un nombre de bits définis (10 ou 12 pour le dsPIC).  
Cela implique que l'ADC réalise une double quantification :

- Quantification dans le temps : c’est l’échantillonnage
- Quantification en niveau : le nombre de valeurs que peut prendre le signal échantillonné est fini

Le rendu d’un convertisseur numérique-analogique dépend donc de deux paramètres :

- La fréquence d’échantillonnage
- La résolution, c’est-à-dire le nombre de bits *N* codant la valeur nuémrique obtenue.  
Elle peut également être définie comme étant la plus petite variation de tension détectable : si le convertisseur a une plage de fonctionnement de 0V à 5V et convertit la grandeur sur 10 bits, on a :
$$
1 LSB = \frac{5V}{2^{10}} = 4,88mV
$$
1 LSB (Least Significant Bit) est la plus petite variation de tension détectable par l'ADC.

- Calculez la résolution de l'ADC du dsPIC sachant que la plage de tension d’entrée est de 0→3,3V et qu’il convertit sur 12 bits.

Le principe de l'échantillonnage et de la conversion est décrit dans la section 16.3 du manuel de référence (sur l'UV).

## Convertisseur numérique-analogique

Un convertisseur numérique analogique (DAC) réalise la fonction inverse d’un ADC.

Notre dsPIC possède un DAC spécialisé pour les applications audio.  Il est assez complexe à utiliser et peu adapté à un usage "simple".  
Nous allons donc utiliser un DAC externe : le **MCP4821**.  Il s’interface avec le µC au travers du bus **SPI**.
 
## Réalisation d'une conversion ADC-DAC

Pour illustrer le fonctionnement des convertisseurs, nous allons réaliser un montage qui convertit un signal analogique en numérique, puis le reconvertit en analogique, à 1 kHz.
Le schéma de principe de notre montage est :

![](./img/ADC-DAC.png){width=75%}

On utilise un suiveur de tension pour protéger l'entrée AN0.  Pour cela, on alimente l’ampli-op (**MCP601**) en 3,3V.  Si la tension appliquée dépasse cet intervalle, l'ampli saturera et la tension appliquée au dsPIC restera dans sa plage admissible.

Nous utiliserons l'ADC interne du dsPIC pour numériser le signal.  
C'est l'un des périphériques les plus compliqués à configurer du dsPIC, car il a beaucoup de modes de fonctionnement.  Nous avons écrit une librairie de fonctions permettant de l'utiliser pour échantillonner uniquement l'entrée AN0.

La librairie supporte 2 modes de fonctionnement :

- Le mode "manuel", dans lequel c'est le code qui déclenche les conversions.
- Le mode "automatique" où c'est le débordement du **TIMER3** qui déclenche les conversions (sans intervention du CPU). 

De même, nous avons écrit une librairie pour le DAC externe.

### Déclenchement manuel

- Réalisez le circuit et programmez le dsPIC avec le projet *adc-dac1.X*.

Pour tester le montage, vous devrez utiliser le générateur de signaux du Picoscope dont l'amplitude maximale est de 2V. Vous pouvez par exemple utiliser un sinus de 1V à 100Hz avec un offset de 1V (votre signal d'entrée doit toujours être positif).

Le projet utilise l’ADC en mode manuel. La période d’échantillonnage est fixée à 1 ms par le **TIMER3**.

Il est configuré pour déclencher son ISR, qui lance la conversion.

La boucle principale fait du polling sur la fin de la conversion.  Lorsqu’elle se produit, le résultat est lu et envoyé au DAC externe.

### Déclenchement par le **TIMER3**

Le projet *adc-dac2.X* réalise la même fonction que le précédent, mais en utilisant le déclenchement automatique de l’ADC par le **TIMER3**.

- Comparez le code des 2 projets.  Quel est l’avantage du second ?
