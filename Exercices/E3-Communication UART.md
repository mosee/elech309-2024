# Communication UART

## Objectifs

Le but de cet exercice est de vous familiariser avec 2 nouveaux concepts dont vous aurez besoin dans le projet et les exercices suivants :

- La communication série asynchrone (UART)
- Le Peripheral Pin Select qui est une fonctionnalité de notre dsPIC.

## Exercice

L'exercice qui vous est demandé consiste à câbler et tester une communication série entre votre dsPIC et un ordinateur.  
Cette section vous donne les informations nécessaires pour faire fonctionner cette communication.  
Les sections suivantes vous expliqueront comment elle fonctionne.

Un test classique pour un canal de communication est de réaliser un écho : 

- L'ordinateur émet une donnée
- Le dsPIC la reçoit et la renvoie à l'ordinateur
- L'ordinateur reçoit "l'écho" du dsPIC.

L'information a alors parcouru le canal de communication, dans les 2 sens.

### Hardware

![](./img/dsPIC-UART.png)

Pour connecter le dsPIC à l'ordinateur, on utilise le module USB-UART *LC234x*.  
Sa patte **VBUS** fournit le 5V de l'USB qui peut être utilisé pour alimenter notre module (il n'est alors  ).  
Evidemment, il faut connecter les masses des 2 modules ensemble.

Ses pattes **TxD** et **RxD** sont connectées à **RB6** et **RB7**.  
Nous n'utiliserons pas les pattes **CTS** et **RTS**.

### Software

#### dsPIC

Le code du projet (*echo.X*) est dans le répertoire [Software/UART](../Software/UART/).  

#### Ordinateur

En général, il faut également écrire du code pour gérer la communication du côté de l'ordinateur.  
Toutefois, il existe des applications permettant d'utiliser facilement les ports série : les moniteurs série.  
Il en existe de nombreux.  Par exemple, [Termite](https://www.compuphase.com/software_termite.htm) est gratuit et simple d'utilisation.

![](./img/termite.png)

Les moniteurs série interprètent les octets comme du texte.  Les données reçues sont affichées dans la fenêtre de réception.  
Pour envoyer, il suffit de taper du texte dans la fenêtre d'émission.

Le bouton de connexion/déconnexion affiche les paramètres utilisés : ici, le module USB-UART est connecté au port **COM6**, le *baud rate* est 57,6kb/s, le format de trame est **8N1** et on n'utilise pas de protocole de handshake (gestion de flux).

On peut modifier ces paramètres avec Settings :

![](./img/termite-settings.png)

L'identifiant du port COM sera probablement différent pour vous.  
Vous pouvez tester les ports proposés dans le menu déroulant.  
Vous pouvez également trouver le port associé au module dans le gestionnaire de périphérique de Windows.

## UART

L'UART est le plus ancien protocole de communication utilisé pour faire communiquer 2 appareils.  
Il est né dans les années 1960 et équipait tous les ordinateurs personnels jusqu'à la fin des années 1990 (sous le nom de RS-232), avant d'être remplacé par l'USB.  
Le standard RS-232 est encore fort utilisé dans le milieu industriel.  Il reste un bon choix pour les systèmes nécessitant la mise en œuvre d’une communication simple, rapide à mettre en œuvre et bon marché.

La version la plus simple d'une transmission RS-232 utilise trois fils (**Rx**-**Tx**-**GND**).  

![](./img/UART-connections.png)

Les signaux de réception **Rx** et de transmission **Tx** sont croisés pour que le **Tx** d'un UART soit connecté au **Rx** de l’autre.  
Chaque UART peut simultanément émettre des données sur sa sortie **Tx** et en recevoir sur son entrée **Rx**.  Ce type de transmission est appelée *Full Duplex*, car les deux interlocuteurs peuvent parler en même temps.

Le terme *asynchrone* signifie qu'il n'y a pas de synchronisation au niveau du bit.  
Cela évite d'avoir à transmettre un signal d'horloge en plus des signaux de données.

Il y a toutefois une synchronisation au niveau de la trame, grâce au *start bit*.  
Le récepteur synchronise son horloge interne sur le début du start bit.  Si la différence de fréquence entre les horloges de l'émetteur et du récepteur est suffisamment faible (<5%), le récepteur peut décoder la trame.

Pour cela, le *baud rate* (fréquence d'émission des bits) et le format de transmission (la composition du mot) doivent être connus à l'avance.  Il existe plusieurs formats possibles pour l'UART.

![Exemples de chronogramme d'une trame UART](./img/UART-frame.png)

Le format d'une trame est défini par :

- L'état de repos (IDLE) du signal est 1.
- La trame débute par le start bit qui vaut toujours 0.  Le récepteur peut donc détecter le début d'une trame en voyant un flanc descendant.
- Le start bit est suivi des bits de donnée à transmettre, en commençant par le bit de poids faible (LSB).  Dans l'immense majorité des cas, il y a 8 bits de données.
Ensuite, vient l'éventuel bit de parité. Il existe 3 possibilités :
  - La plus simple : il n'y a pas de bit de parité dans la trame.  C'est notre cas
  - Le bit de parité ajouté assure une parité paire de la trame (even parity) : l'ensemble des bits de données et celui de parité contient un nombre pair de 1.
  - Le bit de parité ajouté assure une parité impaire de la trame (odd parity) : l'ensemble des bits de données et celui de parité contient un nombre impair de 1  
La parité permet la détection d'une erreur simple : si un seul bit a été altéré lors de la transmission, le récepteur s'en apercevra.  Il ne sera pas capable de déterminer quel bit est erroné.  Une erreur double est indétectable.
- La trame se termine par un stop bit qui assure que le signal est dans l'état par défaut à la fin de la trame.  La durée du stop bit est en général la même que les autres bits, mais il est prévu dans la norme qu'il puisse durer 1,5 ou 2 fois la durée définie par le baud rate (cela laisse plus de temps au récepteur pour traiter une trame avant le début de la suivante).  Le dsPIC supporte un stop bit de longueur 1 ou 2.

Les exemples ci-dessus montre 2 transmissions du même octet, avec 2 formats différents :

- Le 1er n'utilise pas de bit de parité et 1 seul stop bit. C'est celui que nous utiliserons. On l'appelle 8N1 (8 bits, No parity, 1 stop bit).
- Le 2ème utilise une parité paire et 1 stop bit, soit 8E1 (8 bits, Even parity, 1 stop bit).
 
## UART du dsPIC

Notre dsPIC possède 2 périphériques UART.  Voyons comment les utiliser.

### Configuration de l'UART

L'UART a 3 registres de configuration :

- **UxMODE** : permet de choisir le mode de fonctionnent de l’UART.
- **UxSTA** : contient des bits de contrôle et de statut.
- **UxBRG** : Registre configurant le baud rate.

### Générateur de baud rate

La 1ère étape est de définir le débit (baud rate) de l'UART.  
L'UART du dsPIC a 2 modes de fonctionnement : le mode standard et le mode high speed.  
Ce dernier permet de travailler à des débits plus élevés, mais rend l'UART plus sensible au bruit.  Il est donc conseillé d'utiliser le mode standard lorsque c'est possible.  
Le mode de fonctionnement est défini par le bit **UxMODEbits.BRGH**.

Le baud rate est défini par la valeur du registre **UxBRG**.

Pour **BRGH** = 0, la valeur à donner à **UxBRG** est définie par :

$$
UxBRG=\frac{F_CY}{16*baud rate}-1
$$

Où *FCY* est la fréquence du cycle-machine.

Lorsque **BRGH** = 1, la valeur à donner à **UxBRG** est définie par  :

$$
UxBRG=\frac{F_CY}{4*baud rate}-1
$$

Dans le cas (fréquent) où le résultat n'est pas un nombre entier, arrondissez-le à l'entier le plus proche pour minimiser l'erreur.  A ce propos, sachez qu'en C, un nombre est **toujours** arrondi à l'entier inférieur lors des calculs.

### Procédure d’initialisation de l’UART

- Initialiser **UxBRG** pour le baud rate désiré (vérifier que l’arrondi ne crée pas une erreur de baud rate généré trop importante).
- Configurer le format des données (UxMODE).
- Activer l’UART (UxMODE).
- Activer la transmission (UxSTA).

Les deux derniers points doivent être impérativement réalisés dans cet ordre.

### Réception de caractères

Voici le schéma-bloc du récepteur UART, donné dans la datasheet du dsPIC.

![schéma-bloc du récepteur UART](./img/UART-block-diagram.png)

La donnée arrivant sur la patte **UxRX** du dsPIC est directement transférée dans le registre à décalage **UxRSR**.  
Ce dernier transforme le bus série en un bus parallèle.  La donnée est ensuite placée dans une mémoire tampon *FIFO* de longueur 4.

Le bit **UxSTAbits.URXDA** (*UART Rx Data Available*) est alors activé pour indiquer qu'une donnée est disponible.  
Ce bit n'est accessible qu'en lecture.  Il est remis à 0 par l'UART lorsque la FIFO est vide.  
Une lecture dans **UxRXREG** permet de récupérer la plus ancienne donnée de la FIFO.

Lorsque la FIFO est remplie, une cinquième donnée peut encore être reçue dans **UxRSR**, avant qu’une erreur d’écrasement ne soit déclenchée.  
Si la réception de la 5ème donnée se termine avant que la 1ère donnée de la FIFO ne soit lue :

- Elle n'est pas transférée dans la FIFO (et est donc perdue).
- **UxSTAbits.OERR** est activé pour indiquer une erreur d’écrasement.

Tant que ce bit n'est pas remis à 0 par le programme, l'UART ne reçoit plus de nouvelles données.  Toutefois, le remettre à zéro entraine une réinitialisation de la FIFO, il faut donc au préalable lire les données de la FIFO si on ne veut pas les perdre.
 
Les erreurs de format et de parité sont également détectées automatiquement par l’UART et sont indiquées grâce respectivement à **UxSTAbits.FERR** et **UxSTAbits.PERR**.  
Comme indiqué sur le schéma-bloc, ces bits sont également enregistrés dans une file FIFO parallèle à celle des données reçues.  Les bits disponibles dans **UxSTA** fournissent donc le statut de réception de la donnée présente dans **UxRXREG**.
 
### Emission d’un caractère

Pour envoyer une donnée, il suffit de l’écrire dans le registre **UxTXREG**.  
L'émetteur de l'UART a une structure similaire à celle du récepteur : **UxTXREG** est l'entrée d'une FIFO de longueur 4 qui alimente un registre à décalage.  
**UxSTAbits.TXBF** (*Tx Buffer Full*) indique si la FIFO est pleine ou non.  
Comme la transmission d'une trame prend beaucoup plus de temps que l'écriture dans un registre, il est toujours préférable de vérifier l’état de ce bit avant d’écrire une nouvelle donnée dans **UxTXREF**.
 
## Le peripheral pin select

L'UART1 est connecté aux pattes **RP6** et **RP7** du dsPIC par le *Peripheral Pin Select (PPS)* du dsPIC.  
Le PPS permet de choisir à quelles pattes du dsPIC on veut connecter les périphériques utilisés, c'est très utile pour les µC qui possèdent peu de pattes.

### Configuration d'une patte en sortie d'un périphérique

Chaque I/O du dsPIC est connectée à la sortie d'un multiplexeur.  
Tous les signaux de sortie des périphériques du dsPIC sont connectées aux entrées de ce multiplexeur.  Il peut donc connecter n'importe laquelle de ces sorties à la patte voulue.

De même, chaque signal d'entrée des périphériques est connecté à un multiplexeur dont les entrées sont connectées aux I/O, permettant de connecter n'importe quelle I/O à n'importe quelle entrée de périphérique.  
Son fonctionnement est expliqué dans la datasheet du dsPIC à la *section 11.6* et dans la *section 30* du manuel de référence.  Leur lecture devrait vous permettre de comprendre les 2 lignes de code liées au PPS.
